#!/usr/bin/env python3

import numpy as np
import os
import functools
import operator
import gzip
import struct
import array
import tempfile
import matplotlib.pyplot as plt

# class permetant de travailler avec la BDD MNIST, peut parser le fichier et prendre les images avec les labels et la data.
class MNIST():
    # all the data type of the IDX format
    DATA_TYPES = {0x08: 'B',
                  0x09: 'b',
                  0x0b: 'h',
                  0x0c: 'i',
                  0x0d: 'f',
                  0x0e: 'd'}
    
    labelOffset = 4 + 4 # magic number (32 int) + number of items (32 int)
    imageOffset = 4 + 4 + 4 + 4 # magic number (32 int) + number of items (32 int) + rows (32 int) + cols (32 int) 
    
    currentTrainImagePointer = 0
    currentTrainLabelPointer = 0
    currentTestImagePointer  = 0
    currentTestLabelPointer  = 0

    trainImageIndex = -1
    trainLabelIndex = -1
    testImageIndex  = -1
    testLabelIndex  = -1

    trainImageData = []
    trainLabelData = []
    testImageData  = []
    testLabelData  = []
    
    imageSize = 28
    
    # dans l'ordre testLabel, testImage, trainLabel, trainImage
    def __init__(self, dataPath = "data/", fileLabel = ["t10k-labels-idx1-ubyte", "t10k-images-idx3-ubyte", "train-labels-idx1-ubyte", "train-images-idx3-ubyte"]) :
        try:
                self.testLabel  = open(dataPath + fileLabel[0], "rb")
                self.testImage  = open(dataPath + fileLabel[1], "rb")
                self.trainLabel = open(dataPath + fileLabel[2], "rb")
                self.trainImage = open(dataPath + fileLabel[3], "rb")
        except IOError:
            print("Error: a file in the MNIST database is not here, or not name the right way")
            
        self.currentTrainImagePointer = self.imageOffset
        self.currentTestImagePointer  = self.imageOffset
        self.currentTrainLabelPointer = self.labelOffset
        self.currentTestLabelPointer  = self.labelOffset

        self.trainImageData = self.parse(self.trainImage)
        self.trainLabelData = self.parse(self.trainLabel)
        self.testImageData = self.parse(self.testImage)
        self.testLabelData = self.parse(self.testLabel)
        
    def parse(self, data):
        # on read le header et check que tout est ok
        header = data.read(4)

        if len(header) != 4:
            print("Invalid IDX file")
            return 0

        zeros, dtype, dim = struct.unpack('>HBB', header)
        
        if (zeros != 0):
            print("Invalid IDX file")
            return 0

        try:
            dtype = self.DATA_TYPES[dtype]
        except KeyError:
            print("Unknown data type")

        dim = struct.unpack('>' + 'I' * dim, data.read(4 * dim))

        rawData = array.array(dtype, data.read())
        rawData.byteswap() # little endian

        return np.array(rawData).reshape(dim)
        
    def getTrainImage(self):
        return self.trainImageData

    def getTrainLabel(self):
        return self.trainLabelData

    def getTestImage(self):
        return self.testImageData

    def getTestLabel(self):
        return self.testLabelData

    def getNextTrainImage(self):
        self.trainImageIndex += 1
        return self.trainImageData[self.trainImageIndex]

    def getNextTrainLabel(self):
        self.trainLabelIndex += 1
        return self.trainLabelData[self.trainLabelIndex]

    def getNextTestImage(self):
        self.testImageIndex += 1
        return self.testImageData[self.testImageIndex]

    def getNextTestLabel(self):
        self.testLabelIndex += 1
        return self.testLabelData[self.testLabelIndex]

def normalize(image):
    maxx = np.amax(image)
    return image/maxx

def plotImage(image, title):
    plt.imshow(image, cmap='gray')
    plt.title(title)
    plt.show()

        
# ce nn est totalement connecté et est représenté sous sa forme matricielle je vais pas membeter avec des
# vrais connexion
class NeuralNetwork():
    entree  = 0
    sorties = 0
    weights = []
    biais   = []

    # entree      poids       biais
    # (1, 786) * (786, 16) + (1, 16)
    # (1,  16) * (16, 16)  + (1, 16)
    # ...
    # (1, 16)  * (16, 10)  + (1, 10)   
    def __init__(self, entree, hidden, sortie):
        # biais de l'entree
        self.biais.append(np.random.rand(1, entree)) 
        # poids de l'entree
        self.weights.append(np.random.rand(entree, hidden[0]))

        # creation des poids et biais pour les couches cachees
        for i in range(len(hidden) - 1): 
            self.biais.append(np.random.rand(1, hidden[i]))
            self.weights.append(np.random.rand(hidden[i], hidden[i+1]))

        # poids et biais pour la derniere couche
        self.biais.append(np.random.rand(1, sortie))
        self.weights.append(np.random.rand(hidden[len(hidden) - 1], sortie))
    
    # on propage une image, retourne l'erreur sous forme d'un array
    # R_n = E_n-1*W_n-1 + B_n-1
    def propagate(self, image, label):
        pass

    # back propage l'erreur dans le reseau
    def retropropagate(self, error):
        pass

    # train le reseau sur la bdd fournie
    def train(self, dataSet, epoch, batch):
        pass

    # test le reseau, retourne un % d'erreur
    def test(self, dataSet):
        pass

    # prend une image au hasard, la montre et affiche le resultat du reseau 
    def cherryTest(self):
        pass

    # enregistre le reseau
    def save(self):
        pass

    # load un reseau enregistre
    def load(self):
        pass

def main():
    db = MNIST()
    nn = NeuralNetwork(28**2, [16 for x in range(16)], 10)
    
if __name__ == "__main__":
    main()
